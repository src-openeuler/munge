Name:                munge
Version:             0.5.16
Release:             1
Summary:             Enables uid & gid authentication across a host cluster
License:             GPLv3+ and LGPLv3+
URL:                 https://dun.github.io/munge/
Source0:             https://github.com/dun/munge/releases/download/munge-%{version}/munge-%{version}.tar.xz
Source1:             create-munge-key
Source2:             munge.logrotate
BuildRequires:       gcc systemd-units zlib-devel bzip2-devel libgcrypt-devel
Requires:            munge-libs = %{version}-%{release}
Requires(pre):       shadow-utils
Requires(post):      systemd
Requires(preun):     systemd
Requires(postun):    systemd
%description
MUNGE (MUNGE Uid 'N' Gid Emporium) is an authentication service for creating
and validating credentials. It is designed to be highly scalable for use
in an HPC cluster environment.
It allows a process to authenticate the UID and GID of another local or
remote process within a group of hosts having common users and groups.
These hosts form a security realm that is defined by a shared cryptographic
key. Clients within this security realm can create and validate credentials
without the use of root privileges, reserved ports, or platform-specific
methods.

%package devel
Summary:             Development files for uid * gid authentication across a host cluster
Requires:            munge-libs%{?_isa} = %{version}-%{release}
%description devel
Header files for developing using MUNGE.

%package libs
Summary:             Runtime libs for uid * gid authentication across a host cluster
%description libs
Runtime libraries for using MUNGE.

%prep
%autosetup -p1
cp -p %{SOURCE1} create-munge-key
cp -p %{SOURCE2} munge.logrotate

%build
%configure  --disable-static --with-crypto-lib=libgcrypt
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
install -p -m 755 create-munge-key %{buildroot}/%{_sbindir}/create-munge-key
install -p -D -m 644 munge.logrotate %{buildroot}/%{_sysconfdir}/logrotate.d/munge
rm %{buildroot}/%{_sysconfdir}/sysconfig/munge
rm %{buildroot}/%{_libdir}/libmunge.la
chmod 700 %{buildroot}%{_var}/lib/munge %{buildroot}%{_var}/log/munge
chmod 700 %{buildroot}%{_sysconfdir}/munge
mkdir -p %{buildroot}%{_var}/run/munge
touch %{buildroot}%{_var}/run/munge/munged.pid

%postun
%systemd_postun_with_restart munge.service

%preun
%systemd_preun munge.service

%pre
getent group munge >/dev/null || groupadd -r munge
getent passwd munge >/dev/null || \
useradd -r -g munge -d %{_var}/run/munge -s /sbin/nologin \
  -c "Runs Uid 'N' Gid Emporium" munge
exit 0

%post
%systemd_post munge.service
%ldconfig_scriptlets   libs

%files
%{_bindir}/munge
%{_bindir}/remunge
%{_bindir}/unmunge
%{_sbindir}/munged
%{_sbindir}/mungekey
%{_sbindir}/create-munge-key
%{_mandir}/man1/munge.1.gz
%{_mandir}/man1/remunge.1.gz
%{_mandir}/man1/unmunge.1.gz
%{_mandir}/man7/munge.7.gz
%{_mandir}/man8/munged.8.gz
%{_mandir}/man8/mungekey.8.gz
%{_unitdir}/munge.service
%attr(0700,munge,munge) %dir  %{_var}/log/munge
%attr(0700,munge,munge) %dir %{_sysconfdir}/munge
%attr(0755,munge,munge) %dir  %{_var}/run/munge/
%attr(0644,munge,munge)    %ghost %{_var}/run/munge/munged.pid
%attr(0700,munge,munge) %dir  %{_var}/lib/munge
%config(noreplace) %{_sysconfdir}/logrotate.d/munge
%doc AUTHORS
%doc JARGON NEWS QUICKSTART README
%doc doc

%files libs
%{_libdir}/libmunge.so.2
%{_libdir}/libmunge.so.2.0.0
%doc COPYING COPYING.LESSER

%files devel
%{_includedir}/munge.h
%{_libdir}/libmunge.so
%{_libdir}/pkgconfig/munge.pc
%{_mandir}/man3/munge.3.gz
%{_mandir}/man3/munge_ctx.3.gz
%{_mandir}/man3/munge_ctx_copy.3.gz
%{_mandir}/man3/munge_ctx_create.3.gz
%{_mandir}/man3/munge_ctx_destroy.3.gz
%{_mandir}/man3/munge_ctx_get.3.gz
%{_mandir}/man3/munge_ctx_set.3.gz
%{_mandir}/man3/munge_ctx_strerror.3.gz
%{_mandir}/man3/munge_decode.3.gz
%{_mandir}/man3/munge_encode.3.gz
%{_mandir}/man3/munge_enum.3.gz
%{_mandir}/man3/munge_enum_int_to_str.3.gz
%{_mandir}/man3/munge_enum_is_valid.3.gz
%{_mandir}/man3/munge_enum_str_to_int.3.gz
%{_mandir}/man3/munge_strerror.3.gz

%changelog
* Mon Nov 11 2024 zhangxingrong <zhangxingrong@uniontech.com> - 0.5.16-1
- Upgrade to version 0.5.16 

* Mon May 8 2023 panxiaohe <pan_xiaohe@hoperun.com> - 0.5.15-1
- Upgrade to version 0.5.15

* Tue Jan 18 2022 SimpleUpdate Robot <tc@openeuler.org> - 0.5.14-1
- Upgrade to version 0.5.14

* Mon Sep 7 2020 luoshengwei <luoshengwei@huawei.com> - 0.5.13-1
- package init
